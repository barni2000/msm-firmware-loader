# msm-firmware-loader

This script is responsible for loading firmware blobs from firmware
partitions on qcom devices. It will make a dir in tmp, mount all of the
interesting partitions there and then symlink blobs to a single dir that can
be then provided to the kernel. (At this time only single additional
directory can be provided)

This script attempts to load everything at runtime and be as generic
as possible between the target devices: It should allow a single rootfs
to be used on multiple different devices as long as all the blobs
are present on dedicated partitions.
(Usually the case, Samsung devices ship all blobs, other devices may miss
venus but that still allows for WiFi and modem to work)
